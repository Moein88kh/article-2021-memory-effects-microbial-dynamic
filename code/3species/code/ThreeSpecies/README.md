## Main_ThreeSpecies

                   This code solves a there species microbial community model
                   described by fractional differential equations:
                   D^mu(Xi)=X_i(bi.Fi-ki.Xi)
                   where Fi=\prod[Kik^n/(Kik^n+Xk^n)], k=1,...,N and k~=i
                   D is the fractional Caputo derivative and mu is its order  

### Inputs                   

        mu - Order of derivatives, [mu_B,mu_R,mu_G]  0<mu(i)=<1, e.g. mu=[1,.2,1];
        ------------------------------------------------------------------
        n -  Hill coefficient, e.g. n=2;
        ------------------------------------------------------------------
        N -  Number of Species, e.g. N=3;
        ------------------------------------------------------------------
        Kij - Interation matrix, e.g. Kij=0.1*ones(N);
        ------------------------------------------------------------------
        Ki - Death rate, e.g. Ki=1*ones(N,1);
        ------------------------------------------------------------------
        T - Final time, e.g. T=600;
        ------------------------------------------------------------------
        x0 - Initial conditions, e.g. x0=[1/3;1/3;1/3];
        ------------------------------------------------------------------
        Perturbation - This is changes of the species growth rates, e.g. Perturbation='OUP';
                       Possible usages 'False', 'Pulse, 'Periodic', 'OUP', and 'OUP_new'                      
                       False: No perturbation
                       Pulse: 2 pulses in (60,100) and (200,330)
                       Periodic: Periodic perturbation with 20 span
                       OUP: Stochastic pertubation used in the paper; requirement: T=<700                          
                       OUP_new: New generating stochastic perturbation
        ------------------------------------------------------------------
        b - Growth rates for cases: False, Pulse, and Periodic, e.g. b=[1, .95, 1.05];


### Outputs
        t - Simulated time interval
        x - Species abundances 
        B - Growth rates including perturbation
        Figures related to the given conditions

  Please, report any problem or comment to :
          moein dot khalighi at utu dot fi
