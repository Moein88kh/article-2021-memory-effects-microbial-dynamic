# Main
   We consider here a 15-species community with equal interactions between 
   species and identical initial abundances for all species, where species 
   growth rates are drawn from N(1,0.01).
 -------------------------------------------------------------------------
                   This code solves an N species microbial community model
                   described by fractional differential equations:
                   D^mu(Xi)=Xi(bi.Fi-ki.Xi)
                   where Fi=\prod[Kik^n/(Kik^n+Xk^n)], k=1,...,N and k~=i
                   D is the fractional Caputo derivative and mu is its order                          
### Inputs                   
        mu - Order of derivatives, e.g. mu=0.7*ones(1,N);  % 1-Memory
                                        Or
                                        mu(Blue)=0.659; %  1-Memory_B
                                        mu(Red)=1;      %  1-Memory_R
                                        mu(Green)=1;    %  1-Memory_G 
        ------------------------------------------------------------------
        n -  Hill coefficient, e.g. n=2;                              
        ------------------------------------------------------------------
        N -  Number of Species (N=3k for specifying to 3 groups), e.g. N=15;
        ------------------------------------------------------------------    
        Kij - Matrix interaction; 
        ------------------------------------------------------------------    
        Ki - Death rate, e.g. Ki=1*ones(N,1); 
        ------------------------------------------------------------------
        T - Final time, e.g. T=700;
        ------------------------------------------------------------------
        X0 - Initial conditions, e.g. X0=1/15*ones(N,1); 
        ------------------------------------------------------------------
        b - Growth rates; e.g. b=ones(N,1);
        
-----------------------------------
### Outputs
        t - Simulated time interval
        x - Species abundances 
